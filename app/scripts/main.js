let voiceEmail = document.getElementById('vEmail');
let voicePassword = document.getElementById('vPass');
let emailExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
let recognition = new SpeechRecognition();
recognition.interimResults = true;


function startSpeaking() {
  recognition.start();
  recognition.addEventListener('result', e => {
    const transcript = Array.from(e.results)
      .map(result => result[0])
      .map(result => result.transcript)
      .join('').replace(/\s/g, "")
    console.log(transcript);
    if(emailExp.test(transcript)){
      voiceEmail.value = transcript;
    }
    else {
      voicePassword.focus();      
      voicePassword.value = transcript;
    }
  });
}



function formValidation() {
 
  var email = document.getElementById('tEmail');
  var pass = document.getElementById('tPass');

 
  if (email.value.length == 0) {
    document.getElementById('head').innerText = '* All fields are mandatory *'; 
    email.focus();
    return false;
  }
 
  if (emailValidation(email, '* Please enter a valid email address *')) {
    if (passValidation(pass, '* Please enter a valid password *')) {
      return true;
    }
  }
  return false;
}

function emailValidation(inputtext, alertMsg) {
  var emailExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (inputtext.value.match(emailExp)) {
    return true;
  } else {
    document.getElementById('p1').innerText = 'Please enter valid email id';
    email.focus();
    return false;
  }
}

function passValidation(inputtext, min, max) {
  var uInput = inputtext.value;
  if (uInput.length >= 5 && uInput.length <= 10) {
    return true;
  } else {
    document.getElementById('p2').innerText = `Please enter between ${min} and ${max} characters`; 
    inputtext.focus();
    return false;
  }
}
